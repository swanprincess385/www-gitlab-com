---
layout: handbook-page-toc
title: "MLOps Primer"
description: "An overview what is MLOps, components and architecture"
canonical_path: "/direction/learn/mlops/primer"
noindex: false
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is MLOps?
MLOps is the infrastructure needed to create, iterate, deploy and monitor Machine Learning models effectively

## What is Machine Learning?

While many definitions exists, we will consider here Machine Learning is a set of tools and algorithms that extract information from a dataset to make predictions with a degree of uncertainty. This predictions can be used to enhance user experience or to drive internal decision making.

## Recommended Resources

- [Machine Learning Engineering](http://mlebook.com/): Great introductory book on all aspects of Machine Learning Engineering
- [Hidden Technical Debt in Machine Learning Systems](https://papers.nips.cc/paper/2015/file/86df7dcfd896fcaf2674f757a2463eba-Paper.pdf): Landmark paper on MLOps
- [MLOps Community](https://mlops.community/): Slack group that brings together people from different backgrounds to discuss ML and MLOps 


## Versioning an ML Model

A machine learning model version is actually result of three different artefacts, and changing any of them can wildly change the output, these artefacts being:

* *Code*

Code refers not only to the algorithm used to train the model (Decision Tree, Neural Networks, etc), but also the process of pulling the necessary data to make predictions and the code that transforms this data into a format that can be consumed by the model (feature transformations). It is imperative that the feature transformation code is exactly the same when training the model and deploying the model

* *Hyperparameters*

These are the parameters passed to the model to change its behaviour and to the feature transformers to get the data into the right shape. Some examples of hyperparameters are the seed used for RNG, the max depth of a Decision Tree, the maximum value for a specific features. This numbers can be hyper parameters are sometimes obtained manually to see what works best, and sometimes in automated ways such as Grid Search. You could also see this as configuration	 parameters.

* *Training Dataset*

The dataset used to generate the model is as important as the code, and the same model with the exact same hyper parameters can have a very different output if trained on different datasets. It is good practice to store the datasets that were used for every model version, which can create problems with large duplicated datasets. Using immutable changelogs as the data source for model training make this process a lot simpler, since one just needs to record the id of the last row used. 

It is also important to store the data set because one of the basic assumptions for an ML model to work is that future or unseen samples are generally similar than the samples on the training data. Keeping a copy of the dataset enables monitoring of possible divergences (See Concept Drift) 

## Architectural Patterns

Although still in its early stages, the MLOps community has been discovering a few patterns that are common when creating an architecture for Machine Learning. This document will describe some of this patterns briefly and create a vocabulary.

We will often use the word `store` from now on, and it merits a definition. A store is a service that allows for an artefact to be stored, searched, versioned, collaborated on and even monitored . It’s a service within the organisation that centralizes evolution and usage of a category of artefacts. In contrast, a `registry` only performs storage and search. So  a `Model Store` contains a `Model Registry`, but not the other way around.

### Pipeline Runner

### Model Store
*SOON*

### Feature Store

#### Underlying problem

- Once the number of models start to grow, so does the number of features and the complexity of transforming those features. 
- If I have two or three models that use the same feature with the same transformation, each model would have to contain the logic to transform that feature
- For large organizations, different deparments might be creating and using the same features without realizing
- Keeping the same feature definition for both training and serving avoids Concept Drift down the road
- Data changes quickly, specially on models that are served through streaming

#### Solution

Feature Store centralizes both how a feature is accessed, but also how it is transformed, and how it is accessed. This way, when creating a new model, Data Scientists can search for already existing features and build upon them, or create a new one with a library of operators. Instead of connecting to the data lake directly, during training and serving all features are retrieved directly from the Feature Store.

A feature store can also be seen as the connection point between MLOps and DataOps.

#### Usage

#### Examples

- [Tecton.ai](https://www.tecton.ai/)
- [Feast](https://feast.dev/) OpenSource, is now part of Kubeflow, and supported by Google as part of Vertex.ai

### Insights Store

*SOON*

### Labeling Service

*SOON*

### Prediction Service

*SOON*

## Stages of the MLOps Architecture

*SOON*


## Dictionary

* *Concept Drift* -> Unexpected worse model performance due to the difference between the data distributions seen during model training and model serving.



