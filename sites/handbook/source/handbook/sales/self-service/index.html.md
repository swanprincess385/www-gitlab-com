---
layout: handbook-page-toc
title: "Self-Service at GitLab"
description: "The Self-Service Team is responsible for delivering a cohesive GTM that enables customers and prospects to self-serve: from discovery and adoption, purchasing, expanding, and managing their GitLab subscription"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
