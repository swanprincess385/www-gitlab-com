---
layout: markdown_page
title: "BitBucket Key Features"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## Issue Tracking

| Tight integration with Jira for issue management |                                                                                                                    |
|:------------------------------------------------:|--------------------------------------------------------------------------------------------------------------------|
|                                                  | Code repos can be linked to projects and issues                                                                    |
|                                                  | Cross tool functions: Create/View/Edit Jira issue in Bitbucket and create Pull Request and Branches in Jira issues |
|                                                  | Jira very popular and highly used for issue management                                                             |

## Collaboration 

| Tight Integration with Trello for team collaboration |                                                     |
|:----------------------------------------------------:|-----------------------------------------------------|
|                                                      | Code repos can be linked to task                    |
|                                                      | Planning and Collaboration can be done in Bitbucket |

## Version Control

| Free unlimited private repos |                                                 |
|------------------------------|-------------------------------------------------|
|                              | Up to 5 collaborators                           |
|                              | Allows public access to repos                   |
|                              | Tight integration with other Atlassian products |
|                              | Pull Request and Code Reviews                   |
|                              | Branch comparisons                              |
|                              | Commit History                                  |
|                              | SOC 2 Reporting                                 |

## Continous Delivery

| Bitbucket Pipelines  |                                                                                |
|----------------------|--------------------------------------------------------------------------------|
|                      | Built into Bitbucket                                                           |
|                      | Bitbucket Cloud Only                                                           |
|                      | Linux based agent (runner)                                                     |
|                      | Language specific templates                                                    |
|                      | Build configurations stored in Bitbucket yml file                              |
|                      | Build status visibility in Jira                                                |
|                      | Automatic unlimited concurrency - <br>run any number of pipelines concurrently |
|                      | Pipeline requires a few clicks to activation/setup                             |
